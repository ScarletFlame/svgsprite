const Icon = ({id, className}) => {
    return (
        <svg
            xmlnsXlink="http://www.w3.org/1999/xlink"
            className={`icon ${className}`}
        >
            <use xlinkHref={`img/sprite/sprite.symbol.svg#${id}`} />
        </svg>
    )
}

const App = () => {
    return <div>
        <div className="title">Без стилей</div>
        <div className="container">
            <Icon id="vk"/>
            <Icon id="fb"/>
        </div>
        <div className="title">Стилизованные</div>
        <div className="container">
            <Icon id="vk" className="vk"/>
            <Icon id="fb" className="fb"/>
        </div>

        <div className="title">Пример stroke</div>
        <div className="container">
            <Icon id="recognition" className="rc"/>
        </div>

        <div className="title">Пример нерабочий</div>
        <div className="container">
            <Icon id="fb-color" className="fb"/>
        </div>
    </div>
}

let domContainer = document.querySelector('#demo');
ReactDOM.render(<App />, domContainer);
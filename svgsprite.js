const SVGSpriter = require('svg-sprite')
const mkdirp = require('mkdirp')
const path = require('path')
const fs = require('fs')
const spriter = new SVGSpriter({
        dest: 'img/sprite',
        shape: {
                id: {
                        generator: function(name, file) {
                                return name.split('.')[0];
                        }
                }
        },
        mode: {
                symbol: {
                        dest: path.join(__dirname, 'img'),
                        sprite: 'sprite/sprite.symbol.svg'
                }
        }
})


const spriteDir = 'icons'
fs.readdirSync(spriteDir).forEach(file => {
        spriter.add(
            path.join(__dirname, spriteDir, file),
            null,
            fs.readFileSync(path.join(spriteDir, file)),
            { encoding: 'utf-8' }
        )
})

spriter.compile(function(error, result, data) {
        for (var type in result.symbol) {
                mkdirp.sync(path.dirname(result.symbol[type].path))
                fs.writeFileSync(result.symbol[type].path, result.symbol[type].contents)
        }
})
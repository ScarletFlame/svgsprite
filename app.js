var Icon = function Icon(_ref) {
    var id = _ref.id,
        className = _ref.className;

    return React.createElement(
        "svg",
        {
            xmlnsXlink: "http://www.w3.org/1999/xlink",
            className: "icon " + className
        },
        React.createElement("use", { xlinkHref: "img/sprite/sprite.symbol.svg#" + id })
    );
};

var App = function App() {
    return React.createElement(
        "div",
        null,
        React.createElement(
            "div",
            { className: "title" },
            "\u0411\u0435\u0437 \u0441\u0442\u0438\u043B\u0435\u0439"
        ),
        React.createElement(
            "div",
            { className: "container" },
            React.createElement(Icon, { id: "vk" }),
            React.createElement(Icon, { id: "fb" })
        ),
        React.createElement(
            "div",
            { className: "title" },
            "\u0421\u0442\u0438\u043B\u0438\u0437\u043E\u0432\u0430\u043D\u043D\u044B\u0435"
        ),
        React.createElement(
            "div",
            { className: "container" },
            React.createElement(Icon, { id: "vk", className: "vk" }),
            React.createElement(Icon, { id: "fb", className: "fb" })
        ),
        React.createElement(
            "div",
            { className: "title" },
            "\u041F\u0440\u0438\u043C\u0435\u0440 stroke"
        ),
        React.createElement(
            "div",
            { className: "container" },
            React.createElement(Icon, { id: "recognition", className: "rc" })
        ),
        React.createElement(
            "div",
            { className: "title" },
            "\u041F\u0440\u0438\u043C\u0435\u0440 \u043D\u0435\u0440\u0430\u0431\u043E\u0447\u0438\u0439"
        ),
        React.createElement(
            "div",
            { className: "container" },
            React.createElement(Icon, { id: "fb-color", className: "fb" })
        )
    );
};

var domContainer = document.querySelector('#demo');
ReactDOM.render(React.createElement(App, null), domContainer);